
/**
 *  nodeXeroInvoice javascript file 
 * Add new contact in form
 */
var nodeXeroInvoice = {};
Drupal.behaviors.nodeXeroInvoice = function (context) {
  var value = $('#' + 'edit-field-xero-contact-value' + ' option:selected').val();
  $('#' + 'edit-field-field_xero_contact-new-contact').css('display', (value == "other") ? 'block' : 'none');
  $.browser.msie == true ? $('#' + 'edit-field-xero-contact-value').click(nodeXeroInvoice.switchField) : $(this).change(nodeXeroInvoice.switchField);
}
nodeXeroInvoice.switchField = function () {
  var value = $('#' + 'edit-field-xero-contact-value' + ' option:selected').val();
  if (value == 'other') {
    $('#' + 'edit-field-field_xero_contact-new-contact').css('display', 'block');
  }
  else {
    $('#' + 'edit-field-field_xero_contact-new-contact').css('display', 'none');
  }
}
