NODE XERO INVOICES
=========================
This module Add features to send XERO invoice when you create a new or edit a content invoice Xero type. 

Install
=========================
1) Simply drop this xero_invoice folder into the modules directory (/sites/all/modules/).
2) Install dependences (content, text, number, date, cck_line_product, xero)
3) Settings XERO module
4) Install via admin/build/modules.

Add node invoice in XERO
=========================
1) Add permissions to the users if necessary (add invoices in xero).
2) Click on create a invoice Xero invoice or simply edit an existing node of this type:
  - Select the contact or create a new
  - Place the start and due date
  - Add Product items 
  - Select Draft invoice option in form to send invoice into the XERO
  - Click on save button

Update invoice node fields settings
=========================
you can change the configuration of the invoice Xero type node fields as any other content type.

Settings XERO api module
=========================
1) Sign up for a free Xero user account (http://www.xero.com/signup).
2) Generate a private and public key using OpenSSL:
  - openssl genrsa -out privatekey.pem 1024 (generates your private key).
  - openssl req -newkey rsa:1024 -x509 -key privatekey.pem -out publickey.cer -days 365 (generates your public key).
3) Login to the Xero Developer Portal (https://api.xero.com) and add new Private Applications.
  - Enter name application.
  - Enter your public key (generated in step 2).
  xero will put at your fingertips:
    - Xero Consumer Key
    - Xero Consumer Secret
4) Download XERO api in http://drupal.org/project/xero
5) Download PHP-Xero in https://github.com/mradcliffe/PHP-Xero.
6) Place xero.php into xero/include folder.
7) Xero Configuration (admin/settings/xero):
  - Xero Consumer Key (generated in step 3).
  - Xero Consumer Secret (generated in step 3).
  - Xero Certificate Path, your publickey.cer file (generated in step 2).
  - Xero Key Path, your privatekey.pem file (generated in step 2).

XERO invoice documentation
=========================
Input format for creating a new invoice (or credit note) see http://blog.xero.com/developer/api/invoices/

